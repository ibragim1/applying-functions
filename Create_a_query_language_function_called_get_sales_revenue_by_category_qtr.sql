CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(p_current_quarter text)
RETURNS TABLE(category text, total_sales_revenue numeric) AS $$
DECLARE
    -- Variable to hold the start date of the input quarter
    v_start_date_of_quarter DATE;
BEGIN
    -- Convert the input quarter and year to the start date of that quarter
    v_start_date_of_quarter := TO_DATE(p_current_quarter || '-01', 'YYYY-Q-MM');

    RETURN QUERY
    SELECT
        c.name,
        SUM(p.amount) AS total_sales_revenue
    FROM
        payment p
    JOIN
        rental r ON p.rental_id = r.rental_id
    JOIN
        inventory i ON r.inventory_id = i.inventory_id
    JOIN
        film f ON i.film_id = f.film_id
    JOIN
        film_category fc ON f.film_id = fc.film_id
    JOIN
        category c ON fc.category_id = c.category_id
    WHERE
        -- Compare the quarter of the payment_date to the quarter derived from p_current_quarter
        date_trunc('quarter', p.payment_date) = date_trunc('quarter', v_start_date_of_quarter)
        AND c.name IS NOT NULL
    GROUP BY
        c.name;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM get_sales_revenue_by_category_qtr('2017-1');

