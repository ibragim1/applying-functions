CREATE OR REPLACE PROCEDURE new_moviee(p_title VARCHAR)
LANGUAGE plpgsql AS $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM language WHERE name = 'Klingon') THEN
        RAISE EXCEPTION 'Language does not exist';
    END IF;
    INSERT INTO film (
        title,
        rental_rate,
        rental_duration,
        replacement_cost,
        release_year,
        language_id
    )
    SELECT
        p_title,
        4.99,
        3,
        19.99,
        EXTRACT(YEAR FROM CURRENT_DATE)::INTEGER,
        language_id
    FROM language
    WHERE name = 'Klingon';
END; $$;

CALL new_moviee('Assasins');

SELECT * FROM film WHERE title = 'Assasins';